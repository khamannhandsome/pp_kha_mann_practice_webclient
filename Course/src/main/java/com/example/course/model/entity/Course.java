package com.example.course.model.entity;

import com.example.course.model.dto.CourseDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    private String courseName;
    private Long courseCode;
    private String description;
    private String instruction;

    public CourseDto toDto(){
        return new CourseDto(
        this.id,
        this.courseName,
        this.courseCode,
        this.description,
        this.instruction
        );
    }



}
