package com.example.course.model.dto;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CourseDto {

    private Long id;
    private String courseName;
    private Long courseCode;
    private String description;
    private String instruction;

    public CourseDto( String courseName,Long courseCode,String description,String instruction){

        this.courseName= courseName;
        this.courseCode= courseCode;
        this.description=description;
        this.instruction= instruction;


    }





}
