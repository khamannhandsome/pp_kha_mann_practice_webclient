package com.example.course.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourseRequest {

    private String courseName;
    private Long courseCode;
    private String description;
    private String instruction;




}
