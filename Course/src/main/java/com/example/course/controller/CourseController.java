package com.example.course.controller;

import com.example.course.model.request.CourseRequest;
import com.example.course.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/course")
public class CourseController {

    private final CourseService courseService;

    @GetMapping("/{courseId}")
    public ResponseEntity<?> getCourseById(@PathVariable Long courseId){
        return ResponseEntity.ok(
                courseService.getCourseById(courseId)
        );
    }

    @GetMapping
    public ResponseEntity<?> getAllCourse(){
        return ResponseEntity.ok(
                courseService.getAllCourse()
        );
    }

    @PostMapping("/create course")
    public ResponseEntity<?> createCourse(@RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok(
                courseService.createCourse(courseRequest)
        );

    }

    @DeleteMapping("/delete course/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable Long id){
        courseService.deleteCourse(id);
        return ResponseEntity.ok(
                "Course delete successfully"
        );
    }

    @PostMapping("/update course/{id}")
    public ResponseEntity<?> updateCourse(@PathVariable Long id,@RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok(
                courseService.updateCourse(id,courseRequest)
        );
    }

}
