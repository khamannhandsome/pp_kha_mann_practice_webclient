package com.example.course.service;

import com.example.course.Repository.CourseRepository;
import com.example.course.model.dto.CourseDto;
import com.example.course.model.entity.Course;
import com.example.course.model.request.CourseRequest;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    public Course getCourseById(Long courseId){
        return courseRepository.findById(courseId).orElseThrow();

    }

    public Course createCourse(CourseRequest courseRequest){

        Course courseTemp= new Course(null,courseRequest.getCourseName(),courseRequest.getCourseCode(),courseRequest.getDescription(),courseRequest.getInstruction());

        return courseRepository.save(courseTemp);
    }


    public List<Course> getAllCourse() {
        return courseRepository.findAll();
    }

    public void deleteCourse(Long id) {
        courseRepository.deleteById(id);
    }


    public CourseDto updateCourse(Long id, CourseRequest courseRequest) {

        if (getCourseById(id) == null) {
            return null;
        } else {
            Course course = getCourseById(id);
            course.setCourseName(courseRequest.getCourseName());
            course.setCourseCode(courseRequest.getCourseCode());
            course.setDescription(courseRequest.getDescription());
            course.setInstruction(courseRequest.getInstruction());
            return courseRepository.save(course).toDto();
        }
    }

    }

