package com.example.student.service;


import com.example.student.model.dto.CourseDto;
import com.example.student.model.dto.StudentDto;
import com.example.student.model.entity.Student;
import com.example.student.model.request.StudentRequest;
import com.example.student.repository.StudentRepository;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository, WebClient.Builder webClient) {
        this.studentRepository = studentRepository;
        this.webClient = webClient.build();
    }

    public Student insertStudent(StudentRequest studentRequest){

        Student studentTemp = new Student(null,studentRequest.getFirstName(),studentRequest.getLastName(),studentRequest.getEmail(),studentRequest.getCreatedDate(),studentRequest.getCourseId());

        studentRepository.save(studentTemp);
      return  studentTemp;
    }

    private final WebClient webClient;

    public StudentDto getStudent(Long id) {

        Student studentTemp= studentRepository.findById(id).orElseThrow();

        CourseDto courseDto= webClient
                .get()
                .uri("http://localhost:7777/api/v1/course/"+studentTemp.getCourseId())
                .retrieve()
                .bodyToMono(CourseDto.class)
                .block();
         StudentDto studentDto= new StudentDto(
                studentTemp.getId(),
                studentTemp.getFirstName(),
                studentTemp.getLastName(),
                studentTemp.getEmail(),
                studentTemp.getCreatedDate(),
                courseDto

        );
        return studentDto;
    }


    public String deleteStudent(Long id) {
        studentRepository.deleteById(id);
        return "Student deleted successfully.";
    }

    public StudentDto updateStudent(Long id,StudentRequest studentRequest) {

        Student studentTemp= studentRepository.findById(id).orElseThrow();
        studentTemp.setFirstName(studentRequest.getFirstName());
        studentTemp.setLastName(studentRequest.getLastName());
        studentTemp.setEmail(studentRequest.getEmail());
        studentTemp.setCreatedDate(studentRequest.getCreatedDate());
        studentTemp.setCourseId(studentRequest.getCourseId());

         StudentDto studentDto =  getStudent(id);

          studentRepository.save(studentTemp);
        return studentDto;
    }


    public List<StudentDto> getAllStudentsWithCourseInfo() {

        List<StudentDto> finalStudentDto =new ArrayList<>();
       List<Student>students =  studentRepository.findAll();
       List<Long> allCourseIdInStudent= new ArrayList<>();

        List<StudentDto> studentDtos= studentRepository.findAll().stream().map(Student::toStudentDto).toList();
        for(Student studentTemp: students){
            allCourseIdInStudent.add(studentTemp.getCourseId());
        }

       for(StudentDto studentDto: studentDtos) {
           studentDto.setId(studentDto.getId());
           studentDto.setFirstName(studentDto.getFirstName());
           studentDto.setLastName(studentDto.getLastName());
           studentDto.setEmail(studentDto.getEmail());
           studentDto.setCreatedDate(studentDto.getCreatedDate());
           CourseDto courseDtoTemp= new CourseDto();

           for(Long courseId: allCourseIdInStudent){
                if(Objects.equals(studentDto.getCourseDto().getId(), courseId)){
                    CourseDto courseDto = webClient
                            .get()
                            .uri("http://localhost:7777/api/v1/course/" +courseId)
                            .retrieve()
                            .bodyToMono(CourseDto.class)
                            .block();
                       courseDtoTemp= courseDto;
                }
           }
           studentDto.setCourseDto(courseDtoTemp);

           finalStudentDto.add(studentDto);
       }


        return finalStudentDto;
    }
}


