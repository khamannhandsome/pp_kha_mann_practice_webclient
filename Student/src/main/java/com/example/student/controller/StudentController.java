package com.example.student.controller;


import com.example.student.model.dto.StudentDto;
import com.example.student.model.entity.Student;
import com.example.student.model.request.StudentRequest;
import com.example.student.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

private final StudentService studentService;


    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("create new student")
    public Student insertStudent(@RequestBody StudentRequest studentRequest){
        return studentService.insertStudent(studentRequest);

    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id){
        return studentService.getStudent(id);
    }

    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable Long id){
        if(studentService.getStudent(id).getId()!= null) {
           return studentService.deleteStudent(id);
        }

        return null;
    }

    @PostMapping("update student/{id}")
    public StudentDto updateStudent(@PathVariable Long id, @RequestBody StudentRequest studentRequest){
        return studentService.updateStudent(id,studentRequest);
    }

    @GetMapping("/get all students")
    public List<StudentDto> getAllStudent(){
        return studentService.getAllStudentsWithCourseInfo();
    }


}
