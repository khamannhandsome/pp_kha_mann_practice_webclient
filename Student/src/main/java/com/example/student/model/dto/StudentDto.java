package com.example.student.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@AllArgsConstructor@NoArgsConstructor
@Builder
@Data
public class StudentDto {


    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Date createdDate;
    private CourseDto courseDto;


    public StudentDto(Long id, String firstName, String lastName, String email, Date createdDate,Long courseId) {
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.createdDate=createdDate;
        this.courseDto=  new CourseDto(courseId);
    }

    public StudentDto(CourseDto courseDto){
        this.courseDto=courseDto;
    }


}
