package com.example.student.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StudentRequest {


    private String firstName;
    private String lastName;
    private String email;
    private Date createdDate;
    private Long courseId;





}
