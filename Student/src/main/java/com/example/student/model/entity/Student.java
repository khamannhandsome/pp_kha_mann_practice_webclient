package com.example.student.model.entity;


import com.example.student.model.dto.CourseDto;
import com.example.student.model.dto.StudentDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@AllArgsConstructor@NoArgsConstructor
@Builder
@Data
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String email;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;
    private Long courseId;


    public StudentDto toStudentDto(){
        return new StudentDto(
                this.id,
                this.firstName,
                this.lastName,this.email,this.createdDate,this.courseId
        );
    }


}