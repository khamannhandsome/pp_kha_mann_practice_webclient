package com.example.student.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CourseDto {

    private Long id;
    private String courseName;
    private Long courseCode;
    private String description;
    private String instruction;


    public CourseDto(Long courseId) {
        this.id=courseId;
    }
}
